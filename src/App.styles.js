import styled from "styled-components";

const BACKGROUND_COLOR = "#281c2d";
const BORDER_COLOR = "#695e93";
const FOCUSED_BORDER_COLOR = "#8155ba";
const RED = "#ba0f30";
const TEXT_COLOR = "#beafc2";

const GoodSignature = styled.div`
  margin-bottom: 10px;
`;

const Textarea = styled.textarea`
  background-color: ${BACKGROUND_COLOR};
  border: 1px solid ${BORDER_COLOR};
  border-radius: 3px;
  color: ${TEXT_COLOR};
  display: block;
  height: 250px;
  margin: 0 0 12px 1px;
  outline: none;
  padding: 4px;
  resize: none;
  width: 432px;

  &:focus,
  &:active,
  &:focus-visible {
    border: 1px solid ${FOCUSED_BORDER_COLOR};
  }
`;

const AddressTextarea = styled(Textarea)`
  height: 16px;
`;

const BadSignature = styled(GoodSignature)`
  color: ${RED};
`;

const Button = styled.button`
  background-color: ${BACKGROUND_COLOR};
  border: 1px solid ${BORDER_COLOR};
  border-radius: 3px;
  color: ${TEXT_COLOR};
  cursor: pointer;
  display: block;
  float: right;
  height: 26px;
  margin: 2px 1px;
  width: 240px;

  ${({ disabled }) => {
    return disabled
      ? `border: 1px solid ${RED};
    cursor: not-allowed;`
      : `&:focus, &:active, &:focus-visible {
      border: 1px solid ${FOCUSED_BORDER_COLOR};
    }`;
  }}
`;

const ButtonsContainer = styled.div`
  display: grid;
  grid-template-columns: auto 240px;
  ${({ rows }) => `grid-template-rows: repeat(${rows || 2}, 32px)`};
  width: 442px;
`;

const Container = styled.div`
  color: ${TEXT_COLOR};
  font-family: mono;
  margin-bottom: 28px;
  padding: 12px;
  width: 444px;
`;

const ErrorContainer = styled.div`
  margin-top: 60px;
  max-width: 432px;
  word-break: break-word;
`;

const ErrorHeader = styled.div`
  color: ${RED};
  margin: 0 0 12px 0;
`;

const InnerContainer = styled.div`
  ${({ marginTop }) => marginTop && `padding-top: ${marginTop};`}
  ${({ marginBottom }) => marginBottom && `padding-bottom: ${marginBottom};`}
`;

const SignedMessageContainer = styled.div`
  word-break: break-word;

  div {
    margin: 0 0 12px 0;
  }
`;

const SubHeader = styled.h2`
  margin-bottom: 15px;
`;

const SubSubHeader = styled.h3`
  margin-bottom: 10px;
`;

export {
  BACKGROUND_COLOR,
  AddressTextarea,
  BadSignature,
  Button,
  ButtonsContainer,
  Container,
  ErrorContainer,
  ErrorHeader,
  GoodSignature,
  InnerContainer,
  SignedMessageContainer,
  SubHeader,
  SubSubHeader,
  Textarea,
};
