import React, { useCallback, useEffect, useState } from "react";
import { utils } from "ethers";
import {
  BACKGROUND_COLOR,
  AddressTextarea,
  BadSignature,
  Button,
  ButtonsContainer,
  Container,
  ErrorContainer,
  ErrorHeader,
  GoodSignature,
  InnerContainer,
  SignedMessageContainer,
  SubHeader,
  SubSubHeader,
  Textarea,
} from "./App.styles";

const BAD = "BAD";
const GOOD = "GOOD";
const NEITHER = "";

const App = () => {
  useEffect(() => {
    const page = document.querySelector("html");
    page.style.backgroundColor = BACKGROUND_COLOR;
  }, []);

  const [account, setAccount] = useState("");
  const [addressToCheck, setAddressToCheck] = useState("");
  const [clearedMessageToSign, setClearedMessageToSign] = useState("");
  const [messageToCheck, setMessageToCheck] = useState("");
  const [messageToSign, setMessageToSign] = useState("");
  const [previousSignedMessage, setPreviousSignedMessage] = useState(null);
  const [signMessageError, setSignMessageError] = useState("");
  const [signatureResult, setSignatureResult] = useState(NEITHER);
  const [signatureToCheck, setSignatureToCheck] = useState("");
  const [signedMessage, setSignedMessage] = useState(null);
  const [waitingForUserSignature, setWaitingForUserSignature] = useState(false);

  const clearMessageToSign = useCallback(() => {
    setClearedMessageToSign(messageToSign);
    setMessageToSign("");
  }, [messageToSign, setClearedMessageToSign, setMessageToSign]);

  const connectWallet = useCallback(async () => {
    const accounts = await window.ethereum?.request({
      method: "eth_requestAccounts",
    });
    setAccount(accounts[0]);
  }, [setAccount]);

  const copySignature = useCallback(() => {
    navigator.clipboard.writeText(JSON.stringify(signedMessage));
  }, [signedMessage]);

  const handleSignAnotherMessageClick = useCallback(
    (e) => {
      setPreviousSignedMessage(signedMessage);
      setSignedMessage(null);
    },
    [setPreviousSignedMessage, setSignedMessage, signedMessage]
  );

  const showPreviousSignedMessage = useCallback(() => {
    setSignedMessage(previousSignedMessage);
    setPreviousSignedMessage(null);
  }, [previousSignedMessage, setPreviousSignedMessage, setSignedMessage]);

  const signMessage = useCallback(
    async (e) => {
      e.preventDefault();
      const hashedMessage = utils.keccak256(
        utils.toUtf8Bytes(messageToSign.trim())
      );
      let signature = "";
      try {
        setWaitingForUserSignature(true);
        signature = await window.ethereum?.request({
          method: "personal_sign",
          params: [hashedMessage, account],
          from: account,
        });
        const _signedMessage = {
          address: account,
          message: messageToSign,
          signature,
        };
        setSignMessageError("");
        setSignedMessage(_signedMessage);
        setWaitingForUserSignature(false);
      } catch (error) {
        const stringifiedError = JSON.stringify(error);
        setSignMessageError(stringifiedError);
        setWaitingForUserSignature(false);
      }
    },
    [account, messageToSign]
  );

  const undoClearMessageToSign = useCallback(() => {
    setMessageToSign(clearedMessageToSign);
    setClearedMessageToSign("");
  }, [clearedMessageToSign, setClearedMessageToSign, setMessageToSign]);

  const updateAddressToCheck = useCallback(
    (e) => {
      setAddressToCheck(e.target.value);
      setSignatureResult(NEITHER);
    },
    [setAddressToCheck]
  );

  const updateMessageToCheck = useCallback(
    (e) => {
      setMessageToCheck(e.target.value);
      setSignatureResult(NEITHER);
    },
    [setMessageToCheck]
  );

  const updateMessageToSign = useCallback(
    (e) => {
      setMessageToSign(e.target.value);
    },
    [setMessageToSign]
  );

  const updateSignatureToCheck = useCallback(
    (e) => {
      setSignatureToCheck(e.target.value);
      setSignatureResult(NEITHER);
    },
    [setSignatureToCheck]
  );

  const verifySignature = useCallback(
    (e) => {
      e.preventDefault();
      try {
        const hashedMessage = utils.arrayify(
          utils.keccak256(utils.toUtf8Bytes(messageToCheck.trim()))
        );
        const signerAddress = utils.verifyMessage(
          hashedMessage,
          signatureToCheck.trim()
        );
        setSignatureResult(
          signerAddress === addressToCheck.trim() ? GOOD : BAD
        );
      } catch (_error) {
        // TODO: surface error
        setSignatureResult(BAD);
      }
    },
    [addressToCheck, messageToCheck, setSignatureResult, signatureToCheck]
  );

  if (!window.ethereum) {
    return <Container>please install a browser wallet</Container>;
  }

  return (
    <Container>
      <InnerContainer marginTop={"20px"} marginBottom={"30px"}>
        <div>
          {account ? (
            <div>{`[ ${account} ]`}</div>
          ) : (
            <Button onClick={connectWallet}>connect 🦊</Button>
          )}
        </div>
      </InnerContainer>
      <InnerContainer marginBottom={"50px"}>
        <SubHeader>sign a message</SubHeader>
        {signedMessage && (
          <SignedMessageContainer>
            <SubSubHeader>signer address:</SubSubHeader>
            <div>{signedMessage?.address || ""}</div>
            <SubSubHeader>signed message:</SubSubHeader>
            <div>{signedMessage?.message || ""}</div>
            <SubSubHeader>signature:</SubSubHeader>
            <div>{signedMessage?.signature || ""}</div>
            <ButtonsContainer rows={2}>
              <div />
              <Button onClick={handleSignAnotherMessageClick}>
                sign another message
              </Button>
              <div />
              <Button onClick={copySignature}>copy signature json</Button>
            </ButtonsContainer>
          </SignedMessageContainer>
        )}
        {!signedMessage && (
          <div>
            <form onSubmit={account ? signMessage : () => {}}>
              <Textarea
                value={messageToSign}
                onChange={updateMessageToSign}
                placeholder={"message to sign"}
              />
              <ButtonsContainer rows={3}>
                <div />
                <Button
                  disabled={!account || waitingForUserSignature}
                  type="submit"
                >
                  {!account && "please connect wallet"}
                  {account &&
                    waitingForUserSignature &&
                    "please approve signature in wallet"}
                  {account && !waitingForUserSignature && "sign"}
                </Button>
                {messageToSign?.length ? (
                  <>
                    <div />
                    <Button onClick={clearMessageToSign}>clear</Button>
                  </>
                ) : null}
                {!messageToSign?.length && clearedMessageToSign?.length ? (
                  <>
                    <div />
                    <Button onClick={undoClearMessageToSign}>restore</Button>
                  </>
                ) : null}
                {previousSignedMessage && (
                  <>
                    <div />
                    <Button onClick={showPreviousSignedMessage}>
                      show last signed message
                    </Button>
                  </>
                )}
              </ButtonsContainer>
            </form>
          </div>
        )}
        {signMessageError && (
          <ErrorContainer>
            <ErrorHeader>error:</ErrorHeader>
            <div>{signMessageError}</div>
          </ErrorContainer>
        )}
      </InnerContainer>
      <InnerContainer marginBottom={"30px"}>
        <SubHeader>verify a signature</SubHeader>
        <div>
          <form onSubmit={verifySignature}>
            <AddressTextarea
              type={"text"}
              value={addressToCheck}
              onChange={updateAddressToCheck}
              placeholder={"signer's address"}
            />
            <Textarea
              value={messageToCheck}
              onChange={updateMessageToCheck}
              placeholder={"signed message"}
            />
            <Textarea
              value={signatureToCheck}
              onChange={updateSignatureToCheck}
              placeholder={"signature"}
            />
            <GoodSignature>
              {signatureResult === GOOD && `the signature is valid`}
            </GoodSignature>
            <BadSignature>
              {signatureResult === BAD && `the signature is invalid`}
            </BadSignature>
            <div>{signatureResult === GOOD && null}</div>
            <Button type="submit">verify</Button>
          </form>
        </div>
      </InnerContainer>
    </Container>
  );
};

export default App;
