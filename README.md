# sign and verify

cryptographically sign a message using your ethereum browser wallet and check the validity of a signature

## ipfs deployment

hash is `QmPZLqkApqkaXMSE3XCtgEuHZ1ppqyy5617zJLFuVBvc8G`

## dev start

```
$ nvm use
$ yarn
$ yarn start
```
